#!/bin/bash
# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

set -e

# If the run command is the default, do some initialization first
if [ "$(which "$1")" = "/usr/local/bin/start-singleuser.sh" ]; then
    # Clone sample notebooks to user's notebook directory.  Assume $NB_USER's work
    # directory if notebook directory not explicitly set.  `git clone` will fail
    # if target directory already exists and is not empty, which likely means
    # that we've already done it, so just ignore.
    : ${NOTEBOOK_DIR:=/home/$NB_USER/work}
    git config --global push.default matching
    git config --global user.email "$JUPYTERHUB_USER@app-learninglab.inria.fr"
    git config --global user.name "$JUPYTERHUB_USER"
    git config --global core.excludesfile '~/.gitignore_global'
    echo '.ipynb_checkpoints' >> ~/.gitignore_global
    if [ ! -f /home/$NB_USER/.git-credentials ]; then
        git config --global credential.helper store
        echo "https://$JUPYTERHUB_USER:$USER_PASSWORD@app-learninglab.inria.fr" > /home/$NB_USER/.git-credentials
    fi
    git clone "https://app-learninglab.inria.fr/moocrr/gitlab/$JUPYTERHUB_USER/$GITLAB_PROJECT" "$NOTEBOOK_DIR" || true
fi

# Run the command provided
exec "$@"