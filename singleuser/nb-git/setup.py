from setuptools import setup, find_packages

try:
    long_desc = open('README.md').read()
except:
    long_desc = ''

setup(
    name="nbgit",
    url="https://github.com/brospars/nbgit",
    author="Benoit Rospars",
    author_email="benoit.rospars@inria.fr",
    version="0.0.1",
    packages=find_packages(),
    install_requires=[
        "jupyter==1",
        "sh==1.12.14"
    ],
    include_package_data=True,
    description="Buttons to make git commands from notebooks",
    long_description=long_desc,
)