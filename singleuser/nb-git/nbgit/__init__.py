from notebook.utils import url_path_join

from .git_utils import PWDHandler
from .git_utils import GitInfosHandler
from .git_utils import GitStatusHandler
from .git_utils import GitCommitHandler
from .git_utils import GitPushHandler
from .git_utils import GitPullHandler

def _jupyter_server_extension_paths():
    return [{
        "module": "nbgit"
    }]


def _jupyter_nbextension_paths():
    """Required to load JS button"""
    return [dict(
        section="notebook",
        src="static",
        dest="nbgit",
        require="nbgit/index")]


def load_jupyter_server_extension(nb_server_app):
    web_app = nb_server_app.web_app
    host_pattern = '.*$'
    web_app.add_handlers(host_pattern, [
        (url_path_join(web_app.settings['base_url'], r'/pwd'), PWDHandler)])
    web_app.add_handlers(host_pattern, [
        (url_path_join(web_app.settings['base_url'], r'/git/infos'), GitInfosHandler)])
    web_app.add_handlers(host_pattern, [
        (url_path_join(web_app.settings['base_url'], r'/git/status'), GitStatusHandler)])
    web_app.add_handlers(host_pattern, [
        (url_path_join(web_app.settings['base_url'], r'/git/commit'), GitCommitHandler)])
    web_app.add_handlers(host_pattern, [
        (url_path_join(web_app.settings['base_url'], r'/git/push'), GitPushHandler)])
    web_app.add_handlers(host_pattern, [
        (url_path_join(web_app.settings['base_url'], r'/git/pull'), GitPullHandler)])
