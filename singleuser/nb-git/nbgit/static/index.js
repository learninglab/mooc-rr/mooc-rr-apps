define([
    'base/js/namespace',
    'jquery',
    'base/js/utils',
    'base/js/dialog',
    'base/js/events'
], function (Jupyter, $, utils, dialog, events) {

    var spinner = $('<p/>').html('<svg width="38" height="38" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#fff"> <g fill="none" fill-rule="evenodd"> <g transform="translate(1 1)" stroke-width="2"> <circle stroke-opacity=".5" cx="18" cy="18" r="18"/> <path d="M36 18c0-9.94-8.06-18-18-18"> <animateTransform attributeName="transform" type="rotate" from="0 18 18" to="360 18 18" dur="1s" repeatCount="indefinite"/> </path> </g> </g></svg>').css({
        'display': 'table-cell',
        'vertical-align': 'middle'
    });
    var spinner_overlay = $('<div/>').append(spinner).css({
        display: 'table',
        position: 'fixed',
        top: 0,
        left: 0,
        width: '100%',
        height: '100%',
        background: 'rgba(0,0,0,0.5)',
        'text-align': 'center',
        'z-index': '999'
    });

    var getCookie = function(name) {
      var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
      if (match) return match[2];
    };

    var loader = function (action) {

        if (action === 'show' && !$('body').find(spinner_overlay).length)
            $('body').append(spinner_overlay);
        else
            spinner_overlay.remove();
    };

    var get_git_status = function () {
        var git_statusUrl = utils.url_path_join(utils.get_body_data('baseUrl'), 'git', 'status')
        loader('show');
        $.getJSON(git_statusUrl, function (data) {
            loader('hide');
            dialog.modal(data)
        })
    };

    /*var get_git_infos = function () {
        var git_statusUrl = utils.url_path_join(utils.get_body_data('baseUrl'), 'git', 'infos')
        loader('show');
        $.getJSON(git_statusUrl, function (data) {
            loader('hide');
            dialog.modal(data)
        })
    };*/

    var open_gitlab = function () {
        var id = Jupyter.notebook.base_url.replace('/moocrr/jupyter/user/', '').replace('/', '');
        var filepath = Jupyter.notebook.notebook_path.replace('work/', '');
        var gitlab_url = 'https://app-learninglab.inria.fr/moocrr/gitlab/'+id+'/mooc-rr/blob/master/'+filepath;

        window.open(gitlab_url,'_blank');
    };

    var share_gitlab = function () {
        var id = Jupyter.notebook.base_url.replace('/moocrr/jupyter/user/', '').replace('/', '');
        var filepath = Jupyter.notebook.notebook_path.replace('work/', '');
        var gitlab_url = 'https://app-learninglab.inria.fr/moocrr/gitlab/'+id+'/mooc-rr/blob/master/'+filepath;

        var input = $('<input/>').attr('type','text').attr('size','40').css({'width':'100%'}).val(gitlab_url);
        var body = $('<div/>').append(
            $("<p/>").html('To see your latest changes, you may need to commit and push your files to GitLab.').css('margin-bottom', '5px'),
            $("<p/>").html('Then share this link :')
        ).append(input);

        dialog.modal({
            title: "Share notebook from GitLab",
            keyboard_manager: IPython.notebook.keyboard_manager,
            body: body,
            buttons : {
                "Cancel": {}
            },
            open : function (event, ui) {
                var input = $(this).find('input[type="text"]');
                input.focus().select().on('click', function(){ this.select(); });
            }
        });
    };

    var get_git_pull = function () {
        var git_statusUrl = utils.url_path_join(utils.get_body_data('baseUrl'), 'git', 'pull')
        loader('show');
        $.getJSON(git_statusUrl, function (data) {
            loader('hide');
            var modal = dialog.modal(data)
            modal.on("hidden.bs.modal", function () {
                location.reload();
            });
        })
    };

    var get_git_commit_push = function () {
        var file_saved = false;
        var git_statusUrl = utils.url_path_join(utils.get_body_data('baseUrl'), 'git', 'status');
        IPython.notebook.save_notebook();
        loader('show');
        events.on('notebook_saved.Notebook', function () {
            if (!file_saved) {
                file_saved = true;
                $.getJSON(git_statusUrl, function (data) {
                    loader('hide');
                    var input = $('<input/>').attr('type','text').attr('size','40').css({'width':'100%'});
                    var body = $('<div/>').append(
                        $("<p/>")
                            .html('Files to commit')
                    ).append(data.body).append(
                        $("<p/>")
                            .html('Enter a commit message')
                    ).append(input);
                    dialog.modal({
                        title: "Commit",
                        keyboard_manager: IPython.notebook.keyboard_manager,
                        body: body,
                        buttons : {
                            "Cancel": {},
                            "OK": {
                                class: "btn-primary",
                                click: function () {
                                    loader('show');

                                    var data = {
                                        message : input.val() || 'no commit message',
                                        _xsrf : getCookie('_xsrf')
                                    };

                                    var git_pushUrl = utils.url_path_join(utils.get_body_data('baseUrl'), 'git', 'push')
                                    $.post(git_pushUrl, data, function (data) {
                                        loader('hide');
                                        dialog.modal(data)
                                    })
                                }
                            }
                        },
                        open : function (event, ui) {
                            var that = $(this);
                            // Upon ENTER, click the OK button.
                            that.find('input[type="text"]').keydown(function (event, ui) {
                                if (event.which === 13) {
                                    that.find('.btn-primary').first().click();
                                    return false;
                                }
                            });
                            that.find('input[type="text"]').focus().select();
                        }
                    });
                })
            }
        });
        events.on('notebook_save_failed.Notebook', function (event, error) {
            dialog.modal({
                title: "Error",
                body: "File save failed",
                buttons : {
                    "OK": {}
                },
                open : function (event, ui) {
                    var that = $(this);
                    // Upon ENTER, click the OK button.
                    that.find('input[type="text"]').keydown(function (event, ui) {
                        if (event.which === 13) {
                            that.find('.btn-primary').first().click();
                            return false;
                        }
                    });
                    that.find('input[type="text"]').focus().select();
                }
            });
        });
    };

    var init_buttons = function () {
        if (!Jupyter.toolbar) {
            $([Jupyter.events]).on("app_initialized.NotebookApp", git_status_button);
            return;
        }
        Jupyter.toolbar.add_buttons_group([{
            label: 'Open in GitLab',
            icon: 'fa-gitlab',
            callback: open_gitlab
        }])
        Jupyter.toolbar.add_buttons_group([{
            label: 'Share',
            icon: 'fa-share-square-o',
            callback: share_gitlab
        }])
        Jupyter.toolbar.add_buttons_group([{
            label: 'Git status',
            icon: 'fa-list',
            callback: get_git_status
        }])
        Jupyter.toolbar.add_buttons_group([{
            label: 'Git pull',
            icon: 'fa-download',
            callback: get_git_pull
        }])
        Jupyter.toolbar.add_buttons_group([{
            label: 'Git commit and push',
            icon: 'fa-upload',
            callback: get_git_commit_push
        }])
    };

    var load_ipython_extension = function () {
        init_buttons();
    };

    return {
        load_ipython_extension: load_ipython_extension,
    };

});