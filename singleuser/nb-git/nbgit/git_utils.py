import sh
import os
from tornado import web
from notebook.base.handlers import IPythonHandler


def _build_msg_json(**kwargs):
    return dict(**kwargs)


os.chdir('/home/jovyan/work')


class PWDHandler(IPythonHandler):
    @web.authenticated
    def get(self):
        cwd = os.getcwd()
        self.write(_build_msg_json(title="PWD", body=cwd))
        self.flush()


class GitInfosHandler(IPythonHandler):
    @web.authenticated
    def get(self):
        msg_json = _build_msg_json(title="Error", body="")
        git = sh.git.bake()
        try:
            user_name = git.config('--global', 'user.name').stdout
            user_email = git.config('--global', 'user.email').stdout
            user_info = "User name : " + user_name.decode('utf-8') + "User email : " + user_email.decode('utf-8')
            remote = git.remote('-v').stdout
            msg_json = _build_msg_json(
                title="Git infos",
                body="<pre>Config:\n"+user_info+"\nRemote:\n"+remote.decode('utf-8')+"</pre>", sanitize=False)
        except sh.ErrorReturnCode as e:
            msg_json['body'] = e.stdout.decode('utf-8')+'\n'+e.stderr.decode('utf-8')+'\n'

        self.write(msg_json)
        self.flush()


class GitStatusHandler(IPythonHandler):
    @web.authenticated
    def get(self):
        git = sh.git.bake(c='color.status=never')
        gs = git.status('--short').stdout
        msg_json = _build_msg_json(title="Git status", body="<pre>"+gs.decode('utf-8')+"</pre>", sanitize=False)
        self.write(msg_json)
        self.flush()


class GitCommitHandler(IPythonHandler):
    @web.authenticated
    def get(self):
        self.flush()


class GitPushHandler(IPythonHandler):
    @web.authenticated
    def post(self):
        msg_json = _build_msg_json(title="Error", body="")
        git = sh.git.bake()
        git.add('--all')
        commit_message = self.get_argument('message', default='no commit message')
        try:
            git.commit(m=commit_message)
        except sh.ErrorReturnCode_1:
            msg_json['body'] = "Nothing to commit"+'\n'
        except sh.ErrorReturnCode as e:
            msg_json['body'] = e.stdout.decode('utf-8')+'\n'+e.stderr.decode('utf-8')+'\n'

        try:
            git.push('origin', 'HEAD')
            msg_json = _build_msg_json(title="Commit & push successful", body="Your repository has been updated")
        except sh.ErrorReturnCode as e:
            msg_json['body'] = e.stdout.decode('utf-8')+'\n'+e.stderr.decode('utf-8')+'\n'

        self.write(msg_json)
        self.flush()


class GitPullHandler(IPythonHandler):
    @web.authenticated
    def get(self):
        msg_json = _build_msg_json(title="Error", body="")
        git = sh.git.bake()
        try:
            git.pull()
            msg_json = _build_msg_json(title="Pull successful", body="Your files have been updated")
        except sh.ErrorReturnCode as e:
            msg_json['body'] = e.stdout.decode('utf-8') + '\n' + e.stderr.decode('utf-8') + '\n'

        self.write(msg_json)
        self.flush()



