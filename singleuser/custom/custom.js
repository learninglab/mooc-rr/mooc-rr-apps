define([
    'base/js/namespace',
    'base/js/promises',
    'base/js/events'
], function (Jupyter, promises, events) {

    // Disable auto save
    Jupyter.notebook.set_autosave_interval(0);

    var getCookie = function(name) {
      var value = '; ' + document.cookie;
      var parts = value.split('; ' + name + '=');
      return parts.length != 2 ?
        undefined : parts.pop().split(';').shift();
    };

    var setCookie = function(name, value, expiryDays, domain, path) {
      var exdate = new Date();
      exdate.setDate(exdate.getDate() + (expiryDays || 365));

      var cookie = [
        name + '=' + value,
        'expires=' + exdate.toUTCString(),
        'path=' + (path || '/')
      ];

      if (domain) {
        cookie.push('domain=' + domain);
      }
      document.cookie = cookie.join(';');
    };

    // hide login and server features
    $("#kernel_logo_widget").css("margin","0");
    $("#login_widget").hide();
    $("#login_widget + span").hide();
    $("#shutdown").hide();
    $(".clusters_tab_link").parent().hide();

    // Cookie consent banner
    var $cnil = $('' +
    '<div id="cookie-banner" class="cookie-banner">\n' +
    '  <div class="container">\n' +
    '    <p>Ce site est un service externe à FUN, il est maintenu et géré par <a href="https://learninglab.inria.fr" target="_blank">Inria Learning Lab</a> et les auteurs du MOOC Recherche Reproductible</p>\n' +
    '    <p>En continuant à naviguer, vous nous autorisez à déposer des cookies à des fins de mesure d\'audience.</p>\n' +
    '    <div class="cookie-banner-actions">\n' +
    '      <a class="btn btn-xs btn-danger" href="https://www.fun-mooc.fr/courses/course-v1:inria+41016+session01bis/courseware">Décliner et fermer</a>\n' +
    '      <button id="cnilBannerConfirm" class="btn btn-xs btn-success">Accepter</button>\n' +
    '    </div>\n' +
    '  </div>\n' +
    '</div>');

    $($cnil).on('click','#cnilBannerConfirm', function () {
        setCookie('ill-cookie-consent',true,365,'','/');
        $cnil.remove();
    });

    getCookie('ill-cookie-consent') === 'true' || $('body').prepend($cnil);

    promises.app_initialized.then(function (appname) {
        if (appname === 'DashboardApp') {
            // dashboard specific
        }

        if (appname === 'NotebookApp') {
            promises.notebook_loaded.then(function () {
            });

            // hide widget menu
            events.on('kernel_ready.Kernel', function () {
                $("#widget-submenu").closest(".dropdown").hide();
            });

            // rephrase the 'checkpoint' thing that is confusing
            $("#save_checkpoint>a").html("Save");
            $('button[title="Save and Checkpoint"]').attr('title', 'Save');
        }
    });
});

// Make custom url to remove user id in url
var pattern = /user\/[a-zA-Z0-9]+\//g;
var url = window.location.href.replace(pattern, "user/");
// Matomo tracking analytics
var _paq = _paq || [];
_paq.push(['setCustomUrl', url]);
_paq.push(['trackPageView']);
_paq.push(['enableLinkTracking']);
(function () {
    var u = "//piwik.inria.fr/";
    _paq.push(['setTrackerUrl', u + 'piwik.php']);
    _paq.push(['setSiteId', '87']);
    var d = document, g = d.createElement('script'), s = d.getElementsByTagName('script')[0];
    g.type = 'text/javascript';
    g.async = true;
    g.defer = true;
    g.src = u + 'piwik.js';
    s.parentNode.insertBefore(g, s);
})();