# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

# Configuration file for JupyterHub
import os
import sys
import hashlib
import requests
import urllib.parse
from dockerspawner import DockerSpawner

c = get_config()

def get_user_password(username):
    key = os.environ['JUPYTERHUB_CRYPT_KEY'] + username
    return hashlib.sha1(key.encode('utf-8')).hexdigest()[:10]

# Custom spawner that extends Dockerspawner
class CustomDockerSpawner(DockerSpawner):
    # Extend get_env to set user password
    def get_env(self):
        env = super().get_env()

        env['USER_PASSWORD'] = get_user_password(self.user.name)
        env['GITLAB_PROJECT'] = os.environ['GITLAB_PROJECT']
        return env

# Spawn single-user servers as Docker containers
c.JupyterHub.spawner_class = CustomDockerSpawner
# Spawn containers from this image
c.DockerSpawner.container_image = os.environ['DOCKER_NOTEBOOK_IMAGE']
# JupyterHub requires a single-user instance of the Notebook server, so we
# default to using the `start-singleuser.sh` script included in the
# jupyter/docker-stacks *-notebook images as the Docker run command when
# spawning containers.  Optionally, you can override the Docker run command
# using the DOCKER_SPAWN_CMD environment variable.
spawn_cmd = os.environ.get('DOCKER_SPAWN_CMD', "start-singleuser.sh")
c.DockerSpawner.extra_create_kwargs.update({ 'command': spawn_cmd })
# Connect containers to this Docker network
network_name = os.environ['DOCKER_NETWORK_NAME']
c.DockerSpawner.use_internal_ip = True
c.DockerSpawner.network_name = network_name
# Pass the network name as argument to spawned containers
c.DockerSpawner.extra_host_config = { 'network_mode': network_name, 'volume_driver': 'local' }
# Explicitly set notebook directory because we'll be mounting a host volume to
# it.  Most jupyter/docker-stacks *-notebook images run the Notebook server as
# user `jovyan`, and set the notebook directory to `/home/jovyan/work`.
# We follow the same convention.
notebook_dir = os.environ.get('DOCKER_NOTEBOOK_DIR') or '/home/jovyan/work'
c.DockerSpawner.notebook_dir = notebook_dir
# Mount the real user's Docker volume on the host to the notebook user's
# notebook directory in the container
c.DockerSpawner.volumes = {'moocrr_jupyterhub_user_{username}': notebook_dir}

# Singleuser container prefix (new and deprecated)
c.DockerSpawner.container_prefix = 'moocrr-user'
c.DockerSpawner.prefix = 'moocrr-user'

# Remove containers once they are stopped
c.DockerSpawner.remove_containers = True
# For debugging arguments passed to spawned containers
c.DockerSpawner.debug = True
# Set a memory limit
c.DockerSpawner.mem_limit = '500M'



# User containers will access hub by container name on the Docker network
c.JupyterHub.hub_ip = 'moocrr_jupyterhub'
c.JupyterHub.hub_port = 8080
c.JupyterHub.base_url = os.environ.get('MOOC_RR_HUB_PATH')

# Whitlelist admins
c.Authenticator.enable_auth_state = True
c.Authenticator.admin_users = admin = set()
c.JupyterHub.admin_access = True
pwd = os.path.dirname(__file__)
with open(os.path.join(pwd, 'userlist')) as f:
    for line in f:
        if not line:
            continue
        parts = line.split()
        name = parts[0]
        if len(parts) > 1 and parts[1] == 'admin':
            admin.add(name)

# Authenticate users with LTI
c.JupyterHub.authenticator_class = 'ltiauthenticator.LTIAuthenticator'
c.LTIAuthenticator.consumers = {
    os.environ['LTI_KEY']:os.environ['LTI_SECRET']
}

def lti_authenticated_cb(auth, username):
    api_url = os.environ['MOOC_RR_PROTOCOL']+os.environ['MOOC_RR_DOMAIN_NAME'] + os.environ['MOOC_RR_GITLAB_PATH'] + '/api/v4/'
    headers = {'PRIVATE-TOKEN': os.environ['GITLAB_API_TOKEN']}

    r = requests.get(api_url+'users?provider=Jupyterhub&extern_uid='+username, headers=headers)
    results = r.json()

    # Create user if not exist
    if not results:
        auth.log.warning("Create user " + username)
        user_data = {
            "name": username,
            "username": username,
            "password": get_user_password(username),
            "email": username+"@app-learninglab.inria.fr",
            "projects_limit": 3,
            "provider": "Jupyterhub",
            "extern_uid": username,
            "can_create_group": False,
            "skip_confirmation": True
        }

        result = requests.post(api_url + 'users?provider=Jupyterhub&extern_uid=' + username, headers=headers, json=user_data)

        user = result.json()

        if "id" not in user:
            raise ValueError("User creation went wrong "+user.message)
        else:
            group_data = {"user_id": user["id"], "access_level": 30}
            requests.post(api_url + 'groups/'+os.environ['GITLAB_SESSION_GROUP']+'/members', headers=headers, json=group_data)
    elif isinstance(results, dict):
        raise ValueError("User json value is a dict (list expected)")
    else:
        user = results[0]
        if "id" not in user:
            raise ValueError("User json value is wrong")

    project_url = api_url + urllib.parse.quote_plus(username+"/"+os.environ['GITLAB_PROJECT'])
    project_json = requests.get(project_url, headers=headers).json()

    if "id" not in project_json:
        try:
            fork_data  = {'namespace': username}
            fork_request = requests.post(api_url + 'projects/'+urllib.parse.quote_plus("learning-lab/"+os.environ['GITLAB_PROJECT'])+'/fork', headers=headers, json=fork_data)
            fork_request.raise_for_status()
        except requests.exceptions.HTTPError as err:
            auth.log.error(err)

c.LTIAuthenticator.lti_authenticated_hook = lti_authenticated_cb

# Persist hub data on volume mounted inside container
data_dir = os.environ.get('DATA_VOLUME_CONTAINER', '/data')

c.JupyterHub.cookie_secret_file = os.path.join(data_dir,
    'jupyterhub_cookie_secret')

c.JupyterHub.db_url = 'postgresql://postgres:{password}@{host}/{db}'.format(
    host=os.environ['POSTGRES_HOST'],
    password=os.environ['POSTGRES_PASSWORD'],
    db=os.environ['POSTGRES_DB'],
)


# External OAuth Consumer
c.JupyterHub.services = [
    {
        'name': 'cull-idle',
        'admin': True,
        'command': 'python3 ./services/cull-idle.py --timeout=3600'.split(),
    },
    {
        'name': 'sso-oauth-bridge',
        'url': 'http://127.0.0.1:10101',
        'command': ['flask', 'run', '--port=10101'],
        'environment': {
            'FLASK_APP': './services/sso-oauth-bridge.py',
            'JUPYTERHUB_CRYPT_KEY': os.environ['JUPYTERHUB_CRYPT_KEY']
        }
    },
    {
        'name': 'gitlab-oauth',
        'oauth_client_id': 'gitlab-oauth',
        'oauth_redirect_uri': os.environ['MOOC_RR_PROTOCOL']+os.environ['MOOC_RR_DOMAIN_NAME'] + os.environ['MOOC_RR_GITLAB_PATH']+'/users/auth/Jupyterhub/callback',
        'api_token': os.environ['JUPYTERHUB_API_TOKEN']
    },
    {
        'name': 'diff',
        'url': 'http://127.0.0.1:10102',
        'command': [sys.executable, './services/diff.py'],
        'environment': {
            'BASE_URL': os.environ['MOOC_RR_PROTOCOL']+os.environ['MOOC_RR_DOMAIN_NAME'],
            'GITLAB_PROJECT': os.environ['GITLAB_PROJECT'],
            'GITLAB_PROJECT_CORRECTIONS': os.environ['GITLAB_PROJECT_CORRECTIONS'],
            'MOOC_RR_GITLAB_PATH': os.environ['MOOC_RR_GITLAB_PATH'],
        }
    },
    {
        'name': 'nbdime',
        'url': 'http://127.0.0.1:10103',
        'command': [sys.executable, '-m', 'nbdime.webapp.nbdimeserver', '--port=10103', '--base-url=' + os.environ.get('MOOC_RR_HUB_PATH') + '/services/nbdime/'],
    },
    {
        'name': 'password',
        'url': 'http://127.0.0.1:10104',
        'command': [sys.executable, './services/password/password.py'],
        'environment': {
            'JUPYTERHUB_CRYPT_KEY': os.environ['JUPYTERHUB_CRYPT_KEY']
        }
    }]
