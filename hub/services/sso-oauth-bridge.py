#!/usr/bin/env python3
"""
Service to make single sign on with GitLab easier
"""

import json
import os
import hashlib

from flask import Flask, redirect, request, Response, make_response

from jupyterhub.services.auth import HubOAuth


prefix = os.environ.get('JUPYTERHUB_SERVICE_PREFIX', '/')

auth = HubOAuth(
    api_token=os.environ['JUPYTERHUB_API_TOKEN'],
    cache_max_age=60,
)

app = Flask(__name__)

@app.route(prefix + 'user')
def user():
    token = request.headers.get('Authorization')

    if not token:
        return Response('Missing token', 401)

    token = token.replace('Bearer ','')
    user = auth.user_for_token(token)

    if not user:
        return Response('Unvalid token : '+token, 401)

    key = os.environ['JUPYTERHUB_CRYPT_KEY'] + user['name']
    password = hashlib.sha1(key.encode('utf-8')).hexdigest()[:10]

    # setup
    gitlab_user = {
        'id': user['name'],
        'email': user['name'] + '@app-learninglab.inria.fr',
        'password': password
    }

    return Response(
        json.dumps(gitlab_user, indent=1, sort_keys=True),
        mimetype='application/json',
    )