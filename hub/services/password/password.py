"""
Service to display user password
"""
import os
import hashlib
from urllib.parse import urlparse
from tornado.ioloop import IOLoop
from tornado.httpserver import HTTPServer
from tornado.web import RequestHandler, Application, authenticated

from jupyterhub.services.auth import HubAuthenticated


class PasswordHandler(HubAuthenticated, RequestHandler):

    @staticmethod
    def _get_user_password(username):
        key = os.environ['JUPYTERHUB_CRYPT_KEY'] + username
        return hashlib.sha1(key.encode('utf-8')).hexdigest()[:10]

    @authenticated
    def get(self):
        user = self.get_current_user()
        self.render("password.html", username=user['name'], password=self._get_user_password(user['name']))

def main():
    app = Application([
        (os.environ['JUPYTERHUB_SERVICE_PREFIX'], PasswordHandler),
        (r'.*', PasswordHandler),
    ])

    http_server = HTTPServer(app)
    url = urlparse(os.environ['JUPYTERHUB_SERVICE_URL'])

    http_server.listen(url.port, url.hostname)

    IOLoop.current().start()


if __name__ == '__main__':
    main()