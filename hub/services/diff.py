"""
Service to display diff (redirect to nbdime if notebook) and construct url for connected user
"""
import os
import ssl
import urllib
from urllib.parse import urlparse
from tornado.ioloop import IOLoop
from tornado.httpserver import HTTPServer
from tornado.web import RequestHandler, Application, authenticated

from jupyterhub.services.auth import HubAuthenticated
from jupyterhub.utils import url_path_join

from sxsdiff import DiffCalculator
from generators.github_returns import GitHubStyledGenerator


class BuildUserURLHandler(HubAuthenticated, RequestHandler):
    @authenticated
    def get(self):
        user = self.get_current_user()

        submission = self.get_argument('submission', None)

        base_url = os.environ['BASE_URL'].replace('jupyterhub', 'gitlab')

        if submission is None:
            self.set_status(422)
            self.finish("<html><body>422 Missing parameter</body></html>")
        else:
            correction_url = base_url + os.environ['MOOC_RR_GITLAB_PATH'] + '/learning-lab/' + submission.replace(os.environ['GITLAB_PROJECT'], os.environ['GITLAB_PROJECT_CORRECTIONS'])
            submission_url = base_url + os.environ['MOOC_RR_GITLAB_PATH'] + '/' + user['name'] + '/' + submission
            redirect_url = os.environ['JUPYTERHUB_BASE_URL']+'services/diff?correction='+correction_url+'&submission='+submission_url
            self.redirect(redirect_url)


class DiffHandler(HubAuthenticated, RequestHandler):
    @staticmethod
    def _readfile_url(url):
        if not url:
            return 'NO FILE'
        try:
            ctx = ssl.create_default_context()
            ctx.check_hostname = False
            ctx.verify_mode = ssl.CERT_NONE
            with urllib.request.urlopen(url, context=ctx) as f:
                raw = f.read()
                return raw.decode('utf-8')
        except urllib.error.HTTPError:
            return 'NO FILE'

    @staticmethod
    def _getfiletype(url):
        path = urlparse(url).path
        return os.path.splitext(path)[1]

    def get(self):
        correction_url = self.get_argument('correction', None)
        submission_url = self.get_argument('submission', None)

        if self._getfiletype(correction_url) == '.ipynb' and self._getfiletype(submission_url) == '.ipynb':
            redirect_url = os.environ['JUPYTERHUB_BASE_URL'] + 'services/nbdime/difftool?base=' + correction_url + '&remote=' + submission_url
            self.redirect(redirect_url)
        else:
            correction = self._readfile_url(correction_url)
            submission = self._readfile_url(submission_url)

            sxsdiff_result = DiffCalculator().run(correction, submission)

            html_result = GitHubStyledGenerator().run(sxsdiff_result)

            self.set_header('content-type', 'text/html')
            self.write(html_result)


def main():
    app = Application([
        (os.environ['JUPYTERHUB_SERVICE_PREFIX'], DiffHandler),
        (url_path_join(os.environ['JUPYTERHUB_SERVICE_PREFIX'], 'buildurl'), BuildUserURLHandler),
        (r'.*', DiffHandler),
    ])

    http_server = HTTPServer(app)
    url = urlparse(os.environ['JUPYTERHUB_SERVICE_URL'])

    http_server.listen(url.port, url.hostname)

    IOLoop.current().start()


if __name__ == '__main__':
    main()