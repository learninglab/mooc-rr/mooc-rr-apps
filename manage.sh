#!/bin/bash

while test $# -gt 0; do
    case "$1" in
        -h|--help)
            echo "Utilitaire de gestion des utilisateurs du MOOC RR"
            echo " "
            echo "./manage.sh [--reset] [--user]"
            echo " "
            echo "options:"
            echo "-h, --help          Help"
            echo "--reset=CIBLE      Reset gitlab : GITLAB, reset jupyter : JUPYTER, reset complet : ALL"
            echo "--user=USER_ID      ID LTI utilisateur"
            exit 0
            ;;
        --reset*)
            RESET=`echo $1 | sed -e 's/^[^=]*=//g'`
            shift
            ;;
        --user*)
            USER_ID=`echo $1 | sed -e 's/^[^=]*=//g'`
            shift
            ;;
        *)
            break
            ;;
    esac
done

if [ -z "$USER_ID" ] ; then
    echo 'Invalid user'
    exit 0
fi

reset_jupyter () {
    echo "Deleting volume and container for user $1"
    docker rm -f jupyter-$1;
    docker volume rm moocrr_jupyterhub_user_$1
}

reset_gitlab () {
    local user=$(curl -s --header "PRIVATE-TOKEN: Dexjkt6WjC3eVdRQASa_" "https://app-learninglab.inria.fr/moocrr/gitlab/api/v4/users?extern_uid=$1&provider=Jupyterhub");
    local user_id_gitlab=$(echo $user | python -c 'import json,sys;obj=json.load(sys.stdin); id= obj[0]["id"] if len(obj) > 0 else "";print id;')

    if [ -z "$user_id_gitlab" ] ; then
        echo "Unknown gitlab user"
    else
        echo "Deleting user $1 : n°$user_id_gitlab"
        curl --header "PRIVATE-TOKEN: Dexjkt6WjC3eVdRQASa_" -X DELETE  "https://app-learninglab.inria.fr/moocrr/gitlab/api/v4/users/$user_id_gitlab?hard_delete=true";
    fi
}

case "$RESET" in
    ALL)
        reset_jupyter $USER_ID
        reset_gitlab $USER_ID
        ;;
    JUPYTER)
        reset_jupyter $USER_ID
        ;;
    GITLAB)
        reset_gitlab $USER_ID
        ;;
    *)
        echo "Unkown reset command"
        exit 1
        ;;
esac