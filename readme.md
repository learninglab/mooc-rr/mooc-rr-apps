# MOOC Recherche Reproductible

This repository contains everything to set up a jupyterhub/gitlab environment.
It was intended the Reproducible Research course on https://fun-mooc.fr.

![Image : Screenshot](docs/assets/mooc_rr_screenshot.png)

## Table of Contents
1. [Prerequisites](#prerequisites)
2. [Installation guide](#installation-guide)
3. [Documentation](#documentation)
    0. [Guide démarrage nouvelle session](/docs/nouvelle-session.md)
    1. [Infrastructure](/docs/infrastructure.md)
    2. [Use cases](/docs/usecases.md)
    3. Docker Compose (@todo)
    5. Jupyterhub (@todo)
    6. Gitlab (@todo)
4. [Troubleshouting](#troubleshouting)
5. [References](#references)

## Prerequisites
- git `sudo apt-get install git`
- make `sudo apt-get install make`
- [docker](https://docs.docker.com/install/linux/docker-ce/ubuntu/) and [docker-compose](https://docs.docker.com/compose/install/)  

Our specific versions *docker 17.12.1* et *docker-compose 1.19.0* :
```shell
sudo apt-get install docker-ce=17.12.1~ce-0~ubuntu
sudo curl -L https://github.com/docker/compose/releases/download/1.19.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```

- Change docker install directory to `/appli/docker`:
	1. Stop docker: `service docker stop`. Verify no docker process is running `ps faux`
	2. Double check docker really isn't running. Take a look at the current docker directory: `ls /var/lib/docker/`
		2.b Make a backup - `tar -zcC /var/lib docker > /mnt/pd0/var_lib_docker-backup-$(date +%s).tar.gz`
	3. Move the /var/lib/docker directory to your new partition: `mv /var/lib/docker /appli/docker`
	4. Make a symlink: `ln -s /appli/docker /var/lib/docker`
	5. Take a peek at the directory structure to make sure it looks like it did before the mv: `ls /var/lib/docker/` (note the trailing slash to resolve the symlink)
	6. Start docker back up `service docker start`
	7. restart your containers

## Installation guide

### Set environment
This command generate the
```shell
make setenv env=[env]
```

### Optionnal (dev) : Self signed certificate

This command generate a self signed certificate to use for development purposes
```shell
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ./secrets/certs/nginx-selfsigned.key -out ./secrets/certs/nginx-selfsigned.crt
```

This command generate Diffie-Helman parameters. This can take a long time:
```shell
openssl dhparam -out ./secrets/certs/dhparam.pem 4096
```

### Build Jupyter notebook image
This make script build the single user server image
```shell
make notebook_image
```

### Build Jupyterhub Docker image
This make script check and create all needed secrets files, docker volumes and docker networks
```shell
make build
```

### Run all services
```shell
docker compose up -d
```
## Documentation
* [Guide new session](/docs/nouvelle-session.md)
* [Infrastructure](/docs/infrastructure.md)
* [Use cases](/docs/usecases.md)
* Docker Compose (@todo)
* Jupyterhub (@todo)
* Gitlab (@todo)

## Troubleshouting

- See all running/exited container related to jupyter `docker ps -a | grep jupyter`
- Access jupyterhub logs : `docker-compose logs hub`
- Access user logs : `docker logs jupyter-[user_id]`

## References :
- [Jupyter notebook](https://jupyter-notebook.readthedocs.io/en/stable/)
- [Jupyterhub](https://jupyterhub.readthedocs.io/en/stable/)
- [Jupyterhub Docker](https://github.com/jupyterhub/jupyterhub-deploy-docker)
- [Jupyterhub LTI Authenticator](https://github.com/jupyterhub/ltiauthenticator)
- [NbHosting de Thierry Parmentelat](https://github.com/parmentelat/nbhosting)
