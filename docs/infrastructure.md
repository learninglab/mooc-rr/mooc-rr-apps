# Infrastruscture

**Based on [`jupyterhub-docker-deploy`](https://github.com/jupyterhub/jupyterhub-deploy-docker) (dépôt officiel de jupyterhub)**

- Pros :
  - "Standard" because based on official images
  - Deploy in minutes thanks to docker
  - No dependencies or requirements and limited configuration


- Cons :
  - No info on performance or load handling

![Image : Solution based on jupyterhub-docker-deploy](assets/mooc_rr_infra_diagram.png)
