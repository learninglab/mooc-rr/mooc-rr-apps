# Use Cases

A collection of sequence diagrams to describe what is happening under the hood

## Access to a notebook  

![Image : Access to a notebook](assets/mooc_rr_usecase_nb.png)

* *¹ LTI request parameters :
  * user_id
  * next parameter (to redirect after authentication, ex : `/jupyterhub/user/me/notebooks/work/index.ipynb`)
  * oauth parameters (timestamp, signature, etc)
  * roles
  * ...  

## Access to Gitlab

![Image : Access to Gitlab](assets/mooc_rr_usecase_gitlab.png)