# Guide démarrage nouvelle session

Ce guide décrit les principales étapes pour reinitialiser les différents 
services pour accueillir une nouvelle session du MOOC RR.

### 1. Backup session

Connect to the server `ssh app-learninglab.inria.fr` then type :

```
# backup volumes (service and user data)
sudo tar -cvpzf /storage/backup-volumes-$(date +%s).tar.gz /storage/docker-volumes/
# backup current config and secrets
sudo tar -zcvpf /appli/mooc_rr_backup_$(date +%d%m%Y).tar.gz /appli/mooc_rr
```

That will create an archive of all data of all users and services in `/storage` drive.

### 2. Change secrets (API keys, LTI, etc)

I recommend changing LTI and oauth (`JUPYTERHUB_API_TOKEN`) secrets in `/secrets`.

### 3. Reset Jupyterhub

Stop and remove jupyterhub containers : 
```bash
# Remove jupyterhub containers without disturbing other containers (nginx, gitlab)
docker-compose stop hub hub-db && docker-compose rm hub hub-db
```
Reset jupyterhub data and postgres volumes : 
```bash
# Manually remove data and postgres files (because docker volume rm won't follow symlink)
sudo rm -rf /storage/docker-volumes/moocrr_jupyterhub_user_*
# Then remove volumes using docker command
sudo docker volume rm $(docker volume ls -q -f "name=moocrr_jupyterhub_user_*")
```

Remove all users volumes :
```bash 
# Manually remove user data files
sudo rm -rf /storage/docker-volumes/jupyterhub-*
# Then remove volumes using docker command
sudo docker volume rm $(docker volume ls -q -f "name=jupyter*")
```

Then recreate volumes using `make build` and restart all services `docker-compose up -d`


### 4. Reset Gitlab accounts

Create new group `sessionX` in gitlab and change `.env`, `.env.prod`, `.env.dev` 
session group variable `GITLAB_SESSION_GROUP`.

Connect to the server `ssh app-learninglab.inria.fr` then enter rails console by entering :
```bash
> cd /appli/mooc_rr
> docker-compose exec gitlab "/bin/bash"
> gitlab-rails console
```

Run this ruby script to remove all user from previous session X (remove any unwanted user from this group, 
before running the script):

**Attention : ce script ne peut pas être annulé**

```ruby
Group.find_by_name('moocrr-sessionX').users.each do |u|
  u.destroy
end
```