# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

include .env

.DEFAULT_GOAL=build

network:
	@docker network inspect $(DOCKER_NETWORK_NAME) >/dev/null 2>&1 || docker network create $(DOCKER_NETWORK_NAME)

volumes:
	@docker volume inspect $(DATA_VOLUME_HOST) >/dev/null 2>&1 || docker volume create --name $(DATA_VOLUME_HOST)
	@docker volume inspect $(DB_VOLUME_HOST) >/dev/null 2>&1 || docker volume create --name $(DB_VOLUME_HOST)
	@docker volume inspect $(GITLAB_VOLUME_CONFIG) >/dev/null 2>&1 || docker volume create --name $(GITLAB_VOLUME_CONFIG)
	@docker volume inspect $(GITLAB_VOLUME_LOGS) >/dev/null 2>&1 || docker volume create --name $(GITLAB_VOLUME_LOGS)
	@docker volume inspect $(GITLAB_VOLUME_DATA) >/dev/null 2>&1 || docker volume create --name $(GITLAB_VOLUME_DATA)

secrets:
	@echo "Create secrets directory"
	@mkdir $@

secrets/postgres.env:
	@echo "Generating postgres password in $@"
	@echo "POSTGRES_PASSWORD=$(shell openssl rand -hex 32)" > $@

secrets/jupyterhub.env:
	@echo "Generating jupyterhub password in $@"
	@printf "JUPYTERHUB_CRYPT_KEY=$(shell openssl rand -hex 32)\nJUPYTERHUB_API_TOKEN=$(shell openssl rand -hex 32)\n" > $@

secrets/lti.env:
	@echo "Generating LTI key and secret in $@"
	@printf "LTI_KEY=$(shell openssl rand -hex 32)\nLTI_SECRET=$(shell openssl rand -hex 32)\n" > $@

hub/userlist:
	@echo "Create empty userlist"
	@touch $@

check-files: secrets secrets/postgres.env secrets/jupyterhub.env secrets/lti.env hub/userlist

pull:
	docker pull $(DOCKER_NOTEBOOK_IMAGE)

.env:
	@echo "No .env file yet"

setenv:
	@if [ -z ${env} ]; then \
		echo "Environment needed : make setenv env=[env]"; \
	elif [ ! -f .env.${env}  ]; then \
		echo "Env file not found : .env.${env}"; \
	else \
		echo "Generating .env from .env.${env}"; \
		cp ".env.${env}" .env; \
	fi

notebook_image: pull singleuser/Dockerfile
	docker build -t $(LOCAL_NOTEBOOK_IMAGE) \
		--build-arg JUPYTERHUB_VERSION=$(JUPYTERHUB_VERSION) \
		--build-arg DOCKER_NOTEBOOK_IMAGE=$(DOCKER_NOTEBOOK_IMAGE) \
		singleuser

build: check-files network volumes
	docker compose build

.PHONY: network volumes check-files pull notebook_image build
